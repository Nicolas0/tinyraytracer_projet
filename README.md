# Tinyraytracer_Projet

Paul-Antoine Bernard & Nicolas Baudon

## Compile

```
cd tinyraytracer_projet
mkdir build
cd build
cmake ..
make 
./tinyratracer -h 
```

```
	-1              Default, afficher la scène de base composée du canard et de "l'etoile de la mort".
	-2              Stereoscopie, afficher la scène de base composée du canard et de "l'etoile de la mort" avec un effet de stereoscopie.
	-3              Parallax, afficher la scène de base composée du canard et de "l'etoile de la mort" avec un effet de parallax.

```
