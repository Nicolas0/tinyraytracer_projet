#define _USE_MATH_DEFINES
#include <cmath>
#include <limits>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "model.h"
#include "geometry.h"

int envmap_width, envmap_height;
std::vector<Vec3f> envmap;
// int envmap_width_death_star, envmap_height_death_star;
// std::vector<Vec3f> envmap_death_star;
Model duck("../duck.obj");

struct Light {
    Light(const Vec3f &p, const float i) : position(p), intensity(i) {}
    Vec3f position;
    float intensity;
};

struct Material {
    Material(const float r, const Vec4f &a, const Vec3f &color, const float spec) : refractive_index(r), albedo(a), diffuse_color(color), specular_exponent(spec) {}
    Material() : refractive_index(1), albedo(1,0,0,0), diffuse_color(), specular_exponent() {}
    float refractive_index;
    Vec4f albedo;
    Vec3f diffuse_color;
    float specular_exponent;
};

Material      ivory(1.0,   Vec4f(0.6,  0.3, 0.1, 0.0),  Vec3f(0.4, 0.4, 0.3),   50.);
Material      glass(1.5,   Vec4f(0.0,  0.5, 0.1, 0.8),  Vec3f(0.6, 0.7, 0.8),  125.);
Material red_rubber(1.0,   Vec4f(0.9,  0.1, 0.0, 0.0),  Vec3f(0.3, 0.1, 0.1),   10.);
Material green_rubber(1.0, Vec4f(0.9,  0.1, 0.0, 0.0),  Vec3f(0.1, 0.3, 0.1),   10.);
Material     mirror(1.0,   Vec4f(0.0,  10.0, 0.8, 0.0), Vec3f(1.0, 1.0, 1.0), 1425.);

struct Sphere {
    Vec3f center;
    float radius;
    Material material;

    Sphere(const Vec3f &c, const float r, const Material &m) : center(c), radius(r), material(m) {}

    bool ray_intersect(const Vec3f &orig, const Vec3f &dir, float &t0) const {
        Vec3f L = center - orig;
        float tca = L*dir;
        float d2 = L*L - tca*tca;
        if (d2 > radius*radius) return false;
        float thc = sqrtf(radius*radius - d2);
        t0       = tca - thc;
        float t1 = tca + thc;
        if (t0 < 0) t0 = t1;
        if (t0 < 0) return false;
        return true;
    }

    bool ray_intersect_death_star(const Vec3f &orig, const Vec3f &dir, float &t0, float &t1) const {
    Vec3f L = center - orig;
    float tca = L*dir;
    float d2 = L*L - tca*tca;
    if (d2 > radius*radius) return false;
    float thc = sqrtf(radius*radius - d2);
    t0 = tca - thc;
    t1 = tca + thc;
    if (t0 < 0  || t1 < 0) return false;
    return true;
    }
};

struct DeathStar {
    Sphere core, hole;

    DeathStar(const Vec3f &c, const float r, const Material &m) : core(Sphere(c,r,m)), hole(Sphere(Vec3f(-5.5, 7.5, -16.5), 2, green_rubber)) {}
};

Vec3f reflect(const Vec3f &I, const Vec3f &N) {
    return I - N*2.f*(I*N);
}

Vec3f refract(const Vec3f &I, const Vec3f &N, const float eta_t, const float eta_i=1.f) { // Snell's law
    float cosi = - std::max(-1.f, std::min(1.f, I*N));
    if (cosi<0) return refract(I, -N, eta_i, eta_t); // if the ray comes from the inside the object, swap the air and the media
    float eta = eta_i / eta_t;
    float k = 1 - eta*eta*(1 - cosi*cosi);
    return k<0 ? Vec3f(1,0,0) : I*eta + N*(eta*cosi - sqrtf(k)); // k<0 = total reflection, no ray to refract. I refract it anyways, this has no physical meaning
}

bool scene_intersect(const Vec3f &orig, const Vec3f &dir, const std::vector<Sphere> &spheres, const std::vector<DeathStar> deathStars, const std::vector<Model> &models, Vec3f &hit, Vec3f &N, Material &material) {
    float spheres_dist = std::numeric_limits<float>::max();
    for (size_t i=0; i < spheres.size(); i++) {
        float dist_i;
        if (spheres[i].ray_intersect(orig, dir, dist_i) && dist_i < spheres_dist) {
            spheres_dist = dist_i;
            hit = orig + dir*dist_i;
            N = (hit - spheres[i].center).normalize();
            material = spheres[i].material;
        }
    }

    float checkerboard_dist = std::numeric_limits<float>::max();
    if (fabs(dir.y)>1e-3)  {
        float d = -(orig.y+4)/dir.y; // the checkerboard plane has equation y = -4
        Vec3f pt = orig + dir*d;
        if (d>0 && fabs(pt.x)<10 && pt.z<-10 && pt.z>-30 && d<spheres_dist) {
            checkerboard_dist = d;
            hit = pt;
            N = Vec3f(0,1,0);
            material.diffuse_color = (int(.5*hit.x+1000) + int(.5*hit.z)) & 1 ? Vec3f(.3, .3, .3) : Vec3f(.3, .2, .1);
        }
    }

    float models_dist = std::numeric_limits<float>::max();
    for (size_t i = 0; i < models.size(); i++) {
        float dist_i;
        Model tmp_model = models[i];
            for (size_t j = 0; j < tmp_model.nfaces(); j++) {
                if(tmp_model.ray_triangle_intersect(j, orig, dir, dist_i) && dist_i < models_dist && dist_i < spheres_dist && dist_i < checkerboard_dist){
                    models_dist = dist_i;

                    hit = orig + dir*dist_i;

                    Vec3f v1 = tmp_model.point(tmp_model.vert(j,1)) - tmp_model.point(tmp_model.vert(j,0));
                    Vec3f v2 = tmp_model.point(tmp_model.vert(j,2)) - tmp_model.point(tmp_model.vert(j,0));

                    N = cross(v1, v2).normalize();
                    material = glass;
                }
            }
    }

float deathstars_dist = std::numeric_limits<float>::max();

    for (size_t i=0; i < deathStars.size(); i++) {
        float hole_dist_0, hole_dist_1;
        float core_dist_0, core_dist_1;

        Sphere core = deathStars[i].core;
        Sphere hole = deathStars[i].hole;

        bool hit_core = core.ray_intersect_death_star(orig, dir, core_dist_0, core_dist_1);
        bool hit_hole = hole.ray_intersect_death_star(orig, dir, hole_dist_0, hole_dist_1);


        if (hit_core && !hit_hole && core_dist_0 < spheres_dist) {
            deathstars_dist = core_dist_0;
            hit = orig + dir*core_dist_0;
            N = (hit - core.center).normalize();
            // printf("core normal: x=%f, y=%f, z=%f\n", N.x, N.y, N.z);
            material = core.material;
        }
        else if (hit_core && hit_hole) {
            if (hole_dist_0 > core_dist_0 && core_dist_0 < spheres_dist) {
                deathstars_dist = core_dist_0;
                hit = orig + dir*core_dist_0;
                N = (hit - core.center).normalize();
                // printf("hole normal: x=%f, y=%f, z=%f\n", N.x, N.y, N.z);
                material = core.material;
            }
            else if (hole_dist_1 <= core_dist_1 && hole_dist_1 > core_dist_0 && hole_dist_1 < spheres_dist) {
                deathstars_dist = hole_dist_1;
                hit = orig + dir*hole_dist_1;
                N = ((hole.center - hit)).normalize();
                material = hole.material;
            }
            else  if (hole_dist_1 <= core_dist_1 && core_dist_0 < spheres_dist) {
                deathstars_dist = core_dist_0;
                hit = orig + dir*core_dist_0;
                N = (hit - core.center).normalize();
                material = core.material;
            }
        }
    }

    return std::min({spheres_dist, checkerboard_dist, models_dist, deathstars_dist})<1000;
}

Vec3f cast_ray(const Vec3f &orig, const Vec3f &dir, const std::vector<Sphere> &spheres, const std::vector<DeathStar> deathStars, const std::vector<Model> &models, const std::vector<Light> &lights, size_t depth=0) {
    Vec3f point, N;
    Material material;

    if (depth>4 || !scene_intersect(orig, dir, spheres, deathStars, models, point, N, material)) {
        int a = std::max(0, std::min(envmap_width -1, static_cast<int>((atan2(dir.z, dir.x)/(2*M_PI) + .5)*envmap_width)));
        int b = std::max(0, std::min(envmap_height-1, static_cast<int>(acos(dir.y)/M_PI*envmap_height)));
        return envmap[a+b*envmap_width]; // background color
    }

    Vec3f reflect_dir = reflect(dir, N).normalize();
    Vec3f refract_dir = refract(dir, N, material.refractive_index).normalize();
    Vec3f reflect_orig = reflect_dir*N < 0 ? point - N*1e-3 : point + N*1e-3; // offset the original point to avoid occlusion by the object itself
    Vec3f refract_orig = refract_dir*N < 0 ? point - N*1e-3 : point + N*1e-3;
    Vec3f reflect_color = cast_ray(reflect_orig, reflect_dir, spheres, deathStars, models, lights, depth + 1);
    Vec3f refract_color = cast_ray(refract_orig, refract_dir, spheres, deathStars, models, lights, depth + 1);

    float diffuse_light_intensity = 0, specular_light_intensity = 0;
    for (size_t i=0; i<lights.size(); i++) {
        Vec3f light_dir      = (lights[i].position - point).normalize();
        float light_distance = (lights[i].position - point).norm();

        Vec3f shadow_orig = light_dir*N < 0 ? point - N*1e-3 : point + N*1e-3; // checking if the point lies in the shadow of the lights[i]
        Vec3f shadow_pt, shadow_N;
        Material tmpmaterial;
        if (scene_intersect(shadow_orig, light_dir, spheres, deathStars, models, shadow_pt, shadow_N, tmpmaterial) && (shadow_pt-shadow_orig).norm() < light_distance)
            continue;

        diffuse_light_intensity  += lights[i].intensity * std::max(0.f, light_dir*N);
        specular_light_intensity += powf(std::max(0.f, -reflect(-light_dir, N)*dir), material.specular_exponent)*lights[i].intensity;
    }
    return material.diffuse_color * diffuse_light_intensity * material.albedo[0] + Vec3f(1., 1., 1.)*specular_light_intensity * material.albedo[1] + reflect_color*material.albedo[2] + refract_color*material.albedo[3];
}

void render(const std::vector<Sphere> &spheres, const std::vector<DeathStar> deathStars, const std::vector<Model> &models, const std::vector<Light> &lights) {
    const int   width    = 1024;
    const int   height   = 768;
    const float fov      = M_PI/3.;
    std::vector<Vec3f> framebuffer(width*height);

    #pragma omp parallel for
    for (size_t j = 0; j<height; j++) { // actual rendering loop
        for (size_t i = 0; i<width; i++) {
            float dir_x =  (i + 0.5) -  width/2.;
            float dir_y = -(j + 0.5) + height/2.;    // this flips the image at the same time
            float dir_z = -height/(2.*tan(fov/2.));
            framebuffer[i+j*width] = cast_ray(Vec3f(0,0,0), Vec3f(dir_x, dir_y, dir_z).normalize(), spheres, deathStars, models, lights);
        }
    }

    std::vector<unsigned char> pixmap(width*height*3);
    for (size_t i = 0; i < height*width; ++i) {
        Vec3f &c = framebuffer[i];
        float max = std::max(c[0], std::max(c[1], c[2]));
        if (max>1) c = c*(1./max);
        for (size_t j = 0; j<3; j++) {
            pixmap[i*3+j] = (unsigned char)(255 * std::max(0.f, std::min(1.f, framebuffer[i][j])));
        }
    }
    stbi_write_jpg("out.jpg", width, height, 3, pixmap.data(), 100);
}

void render_stereo1(const std::vector<Sphere> &spheres, const std::vector<DeathStar> deathStars, const std::vector<Model> &models, std::vector<Light> &lights) {
    const float eyesep   = 0.2;
    const int   delta    = 60; // focal distance 3
    const int   width    = 960+delta;
    const int   height   = 1080;
    const float fov      = M_PI/3.;
    std::vector<Vec3f> framebuffer1(width*height);
    std::vector<Vec3f> framebuffer2(width*height);

    #pragma omp parallel for
    for (size_t j = 0; j<height; j++) { // actual rendering loop
        for (size_t i = 0; i<width; i++) {
            float dir_x =  (i + 0.5) -  width/2.;
            float dir_y = -(j + 0.5) + height/2.;    // this flips the image at the same time
            float dir_z = -height/(2.*tan(fov/2.));
            framebuffer1[i+j*width] = cast_ray(Vec3f(-eyesep/2,0,0), Vec3f(dir_x, dir_y, dir_z).normalize(), spheres, deathStars, models, lights);
            framebuffer2[i+j*width] = cast_ray(Vec3f(+eyesep/2,0,0), Vec3f(dir_x, dir_y, dir_z).normalize(), spheres, deathStars, models, lights);
        }
    }

    if (0) { // draw the white grid
        for (size_t j = 0; j<height; j++) {
            for (size_t i = 0; i<width; i+=width/10) {
                framebuffer2[i+j*width] = Vec3f(1,1,1);
                framebuffer1[i+j*width] = Vec3f(1,1,1);
            }
        }
        for (size_t i = 0; i<width; i++) {
            for (size_t j = 0; j<height; j+=height/10) {
                framebuffer2[i+j*width] = Vec3f(1,1,1);
                framebuffer1[i+j*width] = Vec3f(1,1,1);
            }
        }
    }

    const float k1 = 0.12;
    const float k2 = 0.10;

    const float xc = (width-delta)/2.;
    const float yc = height/2.;
    const float R = std::min(width-delta, height)/2.f;

    std::vector<unsigned char> pixmap((width-delta)*height*3*2);
    for (size_t j = 0; j<height; j++) {
        for (size_t i = 0; i<width-delta; i++) {
            float xd = i;
            float yd = j;
            float r = std::sqrt(pow(xd-xc, 2) + pow(yd-yc, 2))/R;

            int xu = xc+(xd-xc)*(1+k1*pow(r,2)+k2*pow(r,4));
            int yu = yc+(yd-yc)*(1+k1*pow(r,2)+k2*pow(r,4));

            Vec3f c1 (0,0,0), c2(0,0,0);
            if (xu>=0 && xu<width-delta && yu>=0 && yu<height) {
                c1 = framebuffer2[xu+yu*width+delta];
                c2 = framebuffer2[xu+yu*width];
            }

            float max2 = std::max(c2[0], std::max(c2[1], c2[2]));
            if (max2>1) c2 = c2*(1./max2);
            float max1 = std::max(c1[0], std::max(c1[1], c1[2]));
            if (max1>1) c1 = c1*(1./max1);

            for (size_t d=0; d<3; d++) {
                pixmap[(j*(width-delta)*2 + i            )*3+d] = 255*c1[d];
                pixmap[(j*(width-delta)*2 + i+width-delta)*3+d] = 255*c2[d];
            }
        }
    }
    stbi_write_jpg("out.jpg", (width-delta)*2, height, 3, pixmap.data(), 100);
}

void render_stereo2(const std::vector<Sphere> &spheres, const std::vector<DeathStar> deathStars, const std::vector<Model> &models, const std::vector<Light> &lights) {
    const float eyesep   = 0.2;
    const int   delta    = 60; // focal distance 3
    const int   width    = 1024+delta;
    const int   height   = 768;
    const float fov      = M_PI/3.;
    std::vector<Vec3f> framebuffer1(width*height);
    std::vector<Vec3f> framebuffer2(width*height);

    #pragma omp parallel for
    for (size_t j = 0; j<height; j++) { // actual rendering loop
        for (size_t i = 0; i<width; i++) {
            float dir_x =  (i + 0.5) -  width/2.;
            float dir_y = -(j + 0.5) + height/2.;    // this flips the image at the same time
            float dir_z = -height/(2.*tan(fov/2.));
            framebuffer1[i+j*width] = cast_ray(Vec3f(-eyesep/2,0,0), Vec3f(dir_x, dir_y, dir_z).normalize(), spheres, deathStars, models, lights);
            framebuffer2[i+j*width] = cast_ray(Vec3f(+eyesep/2,0,0), Vec3f(dir_x, dir_y, dir_z).normalize(), spheres, deathStars, models, lights);
        }
    }

    std::vector<unsigned char> pixmap((width-delta)*height*3);
    for (size_t j = 0; j<height; j++) {
        for (size_t i = 0; i<width-delta; i++) {
            Vec3f c1 = framebuffer1[i+delta+j*width];
            Vec3f c2 = framebuffer2[i+      j*width];

            float max1 = std::max(c1[0], std::max(c1[1], c1[2]));
            if (max1>1) c1 = c1*(1./max1);
            float max2 = std::max(c2[0], std::max(c2[1], c2[2]));
            if (max2>1) c2 = c2*(1./max2);
            float avg1 = (c1.x+c1.y+c1.z)/3.;
            float avg2 = (c2.x+c2.y+c2.z)/3.;

            pixmap[(j*(width-delta) + i)*3  ] = 255*avg1;
            pixmap[(j*(width-delta) + i)*3+1] = 0;
            pixmap[(j*(width-delta) + i)*3+2] = 255*avg2;
        }
    }
    stbi_write_jpg("out.jpg", width-delta, height, 3, pixmap.data(), 100);
}

int main(int argc, char *argv[]) {
    int n = -1;
    unsigned char *pixmap = stbi_load("../envmap.jpg", &envmap_width, &envmap_height, &n, 0);

    if (!pixmap || 3!=n) {
        std::cerr << "Error: can not load the environment map" << std::endl;
        return -1;
    }
    envmap = std::vector<Vec3f>(envmap_width*envmap_height);
    for (int j = envmap_height-1; j>=0 ; j--) {
        for (int i = 0; i<envmap_width; i++) {
            envmap[i+j*envmap_width] = Vec3f(pixmap[(i+j*envmap_width)*3+0], pixmap[(i+j*envmap_width)*3+1], pixmap[(i+j*envmap_width)*3+2])*(1/255.);
        }
    }
    stbi_image_free(pixmap);
    std::cout<<"height: "<<envmap_height<<", width: "<<envmap_width;
    std::cout<<std::endl;

    std::vector<Sphere> spheres;
    spheres.push_back(Sphere(Vec3f(-3,    0,   -16), 2,      ivory));
    spheres.push_back(Sphere(Vec3f(-1.0, -1.5, -12), 2,      glass));
    spheres.push_back(Sphere(Vec3f( 1.5, -0.5, -18), 3, red_rubber));
    spheres.push_back(Sphere(Vec3f( 7,    5,   -18), 4,     mirror));

    std::vector<DeathStar> deathStars;
    deathStars.push_back(DeathStar(Vec3f(-8, 6, -20), 4.5, green_rubber));

    std::vector<Model> models;
    models.push_back(Model("../duck.obj"));

    Sphere envsphere(Vec3f(0., 0., 0.), 50, red_rubber);

    std::vector<Light>  lights;
    lights.push_back(Light(Vec3f(-20, 20,  20), 1.5));
    lights.push_back(Light(Vec3f( 30, 50, -25), 1.8));
    lights.push_back(Light(Vec3f( 30, 20,  30), 1.7));

    if (argc == 1 || strcmp(argv[1], "-1") == 0) {
        render(spheres, deathStars, models, lights);
    } else if (argc == 2 &&  strcmp(argv[1], "-2") == 0) {
        render_stereo1(spheres, deathStars, models, lights); // Stereoscope
    } else if (argc == 2 &&  strcmp(argv[1], "-3") == 0) {
        render_stereo2(spheres, deathStars, models, lights); // vue 3d en bleu et rouge (Parallax)
    } else if (argc == 2 && ( strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0 )) {
        printf("\n\t-1              Default, afficher la scène de base composée du canard et de \"l'etoile de la mort\".\n");
        printf("\t-2              Stereoscopie, afficher la scène de base composée du canard et de \"l'etoile de la mort\" avec un effet de stereoscopie.\n");
        printf("\t-3              Parallax, afficher la scène de base composée du canard et de \"l'etoile de la mort\" avec un effet de parallax.\n");
        
    } else {
        printf("Erreur: Pour avoir de l'aide utiliser l'option -h ou --help.\n");
    }

    return 0;
}
